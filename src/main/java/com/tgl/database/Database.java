package com.tgl.database;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import com.mysql.cj.xdevapi.DeleteStatement;
public class Database {
	private static List<employee> Employee = new ArrayList<employee>();
	private static String driver = "com.mysql.cj.jdbc.Driver"; 
	private static String url = "jdbc:mysql://10.67.67.186:3306/emp_kevin?characterEncoding=utf8"; 
	private static String user = "root"; 
	private static String password = "123456";
	public static void main(String[] args) throws IOException {
		readfile();
	    try {
	        Class.forName(driver);
	        //insertdata();
	        //addStaff();
	        //deleteStaff();
	        //updateStaff();
	        //queryStaff();
	        sortStaff();
	    }catch(ClassNotFoundException e) { 
	        System.out.println("找不到驅動程式類別"); 
	        e.printStackTrace(); 
	    }
	    catch(Exception e) { 
	        e.printStackTrace(); 
	    }
	
	}
	public static void readfile() throws IOException {
		BufferedReader file = null;
		try {
			file = new BufferedReader(new FileReader("D:\\list"));
			String i;
			try {
				while((i = file.readLine())!= null) {
					String[] token = i.split("\\s+");
					float H = Float.parseFloat(token[0]);
					float W = Float.parseFloat(token[1]);
					employee temp = new employee(H,W, token[2], 
							token[3],token[4],token[5], Bmi(H,W));
					Employee.add(temp);
				}
			}catch(IOException e) {
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		finally{
			file.close();
		}
	}
	public static void insertdata(){
		String insertsql = "INSERT INTO stafftable(Height,Weight,EnglishName,ChineseName,Ext,Email,BMI,CreateDateTime)"
        		+ " VALUES(?,?,?,?,?,?,?,?)";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		try (Connection conn = DriverManager.getConnection(url, user, password);PreparedStatement ps = conn.prepareStatement(insertsql)){
	        for(employee emp : Employee) {
	        	String date = sdf.format(new Date());
	        	String update = sdf.format(new Date());
				ps.setFloat(1, emp.getHeight());
				ps.setFloat(2, emp.getWeight());
				ps.setString(3, emp.getEN());
				ps.setString(4, emp.getCN());
				ps.setString(5, emp.getExt());
				ps.setString(6, emp.getEmail());
				ps.setFloat(7, emp.getBMI());
				ps.setString(8, date);
				ps.execute();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static float Bmi(float H , float W) {
		float bmi = W/((H/100)*(H/100));
		return bmi;
	}
	
	public static void addStaff() {
		Scanner scanner = new Scanner(System.in);
		String insertsql = "INSERT INTO stafftable(Height,Weight,EnglishName,ChineseName,Ext,Email,BMI,CreateDateTime)"
        		+ " VALUES(?,?,?,?,?,?,?,?)";
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		try (Connection conn = DriverManager.getConnection(url, user, password);
			 PreparedStatement ps = conn.prepareStatement(insertsql);){
	        System.out.println("How many data do you want to enter");
	        int num = scanner.nextInt();
	        for(int i = 0; i < num; i++) {
	        	System.out.println("Please enter your Height,Weight,EnglishName,ChineseName,Ext,Email");
	            String date = sdf.format(new Date());
	    		String enterdata = scanner.next();
	    		String[] token = enterdata.split(",");
	    		float H = Float.parseFloat(token[0]);
	    		float W = Float.parseFloat(token[1]);
	    		if(H < 100 || H > 300) {
	    			System.out.println("your Height enter wrong");
	    			break;
	    		}else if(W < 5 || W > 330 ) {
	    			System.out.println("your Weight enter wrong");
	    			break;
	    		}else if(!token[2].matches("[a-zA-Z]+") && !token[2].contains("-")) {
	    			System.out.println("your EnglishName enter wrong");
	    			break;
	    		}else if(!token[3].matches("[\\u4E00-\\u9FA5]+")){
	    			System.out.println("your ChineseName enter wrong");
	    		}else if(token[4].length() != 4) {
	    			System.out.println("your Ext enter wrong");
	    			break;
	    		}else if(!token[5].contains("@transglobe.com.tw")) {
	    			System.out.println("your Email enter wrong");
	    			break;
	    		}else {
	    			System.out.println("your enter is right");
	    		}
	    		employee temp = new employee(H,W, token[2], 
	    				token[3],token[4],token[5], Bmi(H,W));
	    		ps.setFloat(1, temp.getHeight());
	    		ps.setFloat(2, temp.getWeight());
	    		ps.setString(3, temp.getEN());
	    		ps.setString(4, temp.getCN());
	    		ps.setString(5, temp.getExt());
	    		ps.setString(6, temp.getEmail());
	    		ps.setFloat(7, temp.getBMI());
	    		ps.setString(8, date);
	    		ps.execute();
	        }
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static void deleteStaff(){
		String deletesql = "delete from stafftable where ID = ?;";
		try (Connection conn = DriverManager.getConnection(url, user, password);
			 PreparedStatement ps = conn.prepareStatement(deletesql);){
			ps.setInt(1, 9);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static void updateStaff() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		String update = sdf.format(new Date());
		String updatesql = "update stafftable set Height = ? ,UpDateTime = ? where id = ?;";
		try (Connection conn = DriverManager.getConnection(url, user, password);
			 PreparedStatement ps = conn.prepareStatement(updatesql);){
			ps.setFloat(1, 198);
			ps.setString(2, update);
			ps.setInt(3, 14);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static void queryStaff(){
		String querysql = "select * from stafftable where Height = 170 or Weight = 55";
		try (Connection conn = DriverManager.getConnection(url, user, password);
			 PreparedStatement ps = conn.prepareStatement(querysql); ResultSet rs = ps.executeQuery();){
			intoEmployee(rs);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static void sortStaff(){
		String sortsql = "select * from stafftable order by Weight ASC";
		try (Connection conn = DriverManager.getConnection(url, user, password);
			 PreparedStatement ps = conn.prepareStatement(sortsql); ResultSet rs = ps.executeQuery();){
			intoEmployee(rs);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static void intoEmployee(ResultSet rs) throws SQLException {
		while(rs.next()) {
			char[] CN = rs.getString("ChineseName").toCharArray();
			String changename;
			if(CN.length == 4) {
				changename = rs.getString("ChineseName").replace(CN[1], '*').replace(CN[2], '*');
			}else {
				changename = rs.getString("ChineseName").replace(CN[1], '*');
			}
			employee emp = new employee();
			emp.setID(rs.getInt("ID"));
			emp.setHeight(rs.getFloat("Height"));
			emp.setWeight(rs.getFloat("Weight"));
			emp.setEN(rs.getString("EnglishName"));
			emp.setCN(changename);
			emp.setExt(rs.getString("Ext"));
			emp.setEmail(rs.getString("Email"));
			emp.setBMI(rs.getFloat("BMI"));
			System.out.println(emp);
//			System.out.println(rs.getInt("ID") + ", " + rs.getFloat("Height") + ", " + rs.getFloat("Weight") + ", " + rs.getString("EnglishName") + ", " + 
//							   changename + ", " + rs.getString("Ext") + ", " + rs.getString("Email") + ", " + rs.getFloat("BMI"));
		}
	}
}
