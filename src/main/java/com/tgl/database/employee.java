package com.tgl.database;

public class employee {
	private float Height;
	private float Weight;
	private String EN;
	private String CN;
	private String Ext;
	private String Email;
	private int ID;
	private float BMI;
	
	public employee() {
		
	}
	public employee(float Height, float Weight, String EN, String CN, String Ext, String Email, float BMI) {
		this.Height = Height;
		this.Weight = Weight;
		this.EN = EN;
		this.CN = CN;
		this.Ext = Ext;
		this.Email = Email;
		this.BMI = BMI;
	}

	public float getHeight() {
		return Height;
	}

	public void setHeight(float height) {
		Height = height;
	}

	public float getWeight() {
		return Weight;
	}

	public void setWeight(float weight) {
		Weight = weight;
	}

	public String getEN() {
		return EN;
	}

	public void setEN(String eN) {
		EN = eN;
	}

	public String getCN() {
		return CN;
	}

	public void setCN(String cN) {
		CN = cN;
	}

	public String getExt() {
		return Ext;
	}

	public void setExt(String ext) {
		Ext = ext;
	}

	public String getEmail() {
		return Email;
	}

	public void setEmail(String email) {
		Email = email;
	}

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public float getBMI() {
		return BMI;
	}

	public void setBMI(float bMI) {
		BMI = bMI;
	}
	@Override
	public String toString() {
		return "Employee [ID = " + ID + ", H = " + Height + ", W = " + Weight + ", EN = " + EN + 
				", CN = " + CN + ", Ext = " + Ext + ", Email = " + Email + ", BMI = " + BMI + "]";
	}
}
